package com.jenkins.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GardedAssigmentWeek10Application {

	public static void main(String[] args) {
		SpringApplication.run(GardedAssigmentWeek10Application.class, args);
		System.out.println("Hello Dev-Ops");
	}

}
